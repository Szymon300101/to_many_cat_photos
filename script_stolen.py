import urllib.request
import os, time
from selenium import webdriver
from bs4 import BeautifulSoup

MAX_IMAGES = 500

print("What do you want to download?")
download = input()
print("How many?")
img_target = input()
google_graphic = 'https://www.google.com/search?tbm=isch&q=' + download

browser = webdriver.Chrome(executable_path=os.path.realpath(__file__)+r'/../chromedriver.exe')

browser.get(google_graphic)

count = 0
count_all = 0

try:

	while (count < int(img_target)) and (count_all < MAX_IMAGES):
		browser.execute_script("window.scrollBy(0,document.body.scrollHeight)")
		time.sleep(2)

		soup = BeautifulSoup(browser.page_source, 'html.parser')
		img_tags = soup.find_all("img", class_="rg_i")

		for i in img_tags:
			if count < int(img_target):
			    try:
			        urllib.request.urlretrieve(i['src'], os.path.realpath(__file__)+"/../Images/"+download+"_"+str(count)+".jpg")
			        count+=1
			        count_all+=1
			        print("Number of images downloaded = "+str(count),end='\r')
			    except Exception as e:
			    	count_all+=1
			    	pass
	browser.close()		   		
except Exception as e:
		browser.close()		   
		pass
	
 	